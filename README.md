<p align="center">
  <h1 align="center">pacmanfzf</h1>
  <p align="center">
    pacman + fzf selection
  </p>
  <img
    width="1200"
    src="https://codeberg.org/tplasdio/pacmanfzf/raw/branch/main/media/ss1.avif"
    alt="image not loaded"
  />
</p>

## 📰 Description

A script to combine pacman (a package manager for Arch Linux
based distributions) with fzf, to interactively select packages.

## 🚀 Installation

<!--
## Dependencies
- pacman
- [fzf](https://github.com/junegunn/fzf)
- [getoptions](https://github.com/ko1nksm/getoptions)
- pactree
-->

</details>

<details>
<summary>Arch Linux or other pacman distros</summary>

```sh
curl -O https://codeberg.org/tplasdio/pacmanfzf/raw/branch/main/packaging/PKGBUILD-git
PACMAN=yay makepkg -sip PKGBUILD-git  # or use your AUR helper
```

For uninstallation, run `sudo pacman -Rsn pacmanfzf-git`.

_*Looking for an AUR maintainer*_

</details>

## 📟 Usage

```sh
pacmanfzf -Q                 # Select local packages
pacmanfzf -Qe                # Select local explicitly installed packages
pacmanfzf -Ss                # Select remote packages
pacmanfzf -S                 # Select remote packages and synchronize them
pacmanfzf -Ss --pacman paru  # Select remote packages with AUR helper (if it wraps pacman)
pacmanfzf -S --pacman paru   # Select remote packages with AUR helper and sync them
pacmanfzf -R                 # Select remote packages and remove them
pacmanfzf --help             # Show help
man pcamanfzf                # Show manual

# Inside the interface you can use:
Ctrl-h  # Show help
Ctrl-i  # Show package information
Ctrl-d  # Show package dependencies
Ctrl-r  # Show package reverse dependencies
Ctrl-f  # Show files provided by package
```

## 💭 Similar

- [pacui](https://github.com/excalibur1234/pacui)
- [fzpac](https://github.com/sheepla/fzpac)
- [paruz](https://github.com/joehillen/paruz)
- [pacpreview](https://github.com/TheCycoONE/pacpreview)
- More [graphical AUR helpers](https://wiki.archlinux.org/title/AUR_helpers#Graphical)
- More [pacman utilities](https://wiki.archlinux.org/title/Pacman/Tips_and_tricks#Utilities)

## 📝 License

GPL-2.0-or-later
